import { devices, PlaywrightTestConfig } from "@playwright/test";

const config: PlaywrightTestConfig = {
   timeout:90000,
    retries: 0,
    testDir: 'testCases/Playground',
    reporter:'line',
    use: {
        trace: 'off',
        //...devices['Nokia N9'],
        headless: true,
        //viewport: {width: 1280, height: 720},
        ignoreHTTPSErrors: false,
        actionTimeout: 15000,
        video:'retain-on-failure',
        screenshot:'only-on-failure',
        httpCredentials:{
            username:'platform24',
            password:'7592', 
        },
    },
    expect: {
        timeout: 10000
      },
//         projects: [ {
//         name: 'API',
//         use: {browserName:'chromium' }
//     },
    
//     {
//         name: 'API2',
//         use: {browserName:'firefox' }
//     }, 
//     {
//         name: 'API3',
//         use: {browserName:'webkit'  }
     
// }],
}

export default config