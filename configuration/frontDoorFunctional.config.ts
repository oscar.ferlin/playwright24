import { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
  timeout: 900000,
  retries: 0,
  testDir: '../testCases/frontdoor-specific',
  reporter: 'list',
  globalSetup: require.resolve('../testCases/loginCookies/logintoD24Functional'),
  use: {
    storageState:'./testCases/loginCookies/loginCookiesD24Functional.json',
    headless: true,
    //viewport: {width: 1280, height: 720},
    ignoreHTTPSErrors: true,
    actionTimeout: 10000,
    video: 'retain-on-failure',
    screenshot: 'off',
  },
  expect: {
    timeout:10000
    },
  
        projects: [ {
        name: 'chromium',
        use: {browserName:'chromium' }
    }], 
   /* {
        name: 'firefox',
        use: {browserName:'firefox' }
    }, 
    {
        name: 'webkit',
        use: {browserName:'webkit'  } */
     
}
export default config;