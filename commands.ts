import{Myprofile} from "./pageobjects/homepage/Myprofile"
import { PreTriage } from "./pageobjects/triage/Pre-triage"
import { CancelAppointment } from "./pageobjects/homepage/CancelAppointment"
import { Page, expect } from "@playwright/test"
import {Consent} from "./pageobjects/homepage/Consent"
const assert = JSON.parse(JSON.stringify(require("./utils/assertions/assertionsSWE.json")))

export async function confirmContactDetails(page: Page) {
    let myProfile= new Myprofile(page)
    await myProfile.saveContactDetailsBtn.click()
}
export async function seekCareForAdultAndConditionNewPatientModel(page: Page, condition: string) {
    let preTriage=new PreTriage(page)
    await page.waitForLoadState('networkidle')     
    await preTriage.newPatientModelAdult.click()
    await preTriage.inputFieldCondition.type(condition)
    await preTriage.searchResults.first().waitFor()
    for(let i=0;i< await preTriage.searchResults.count();i++) {
        if(await preTriage.searchResults.nth(i).textContent()===condition) {
            await preTriage.searchResults.nth(i).click() }}   
}

export async function seekCareForAdultAndCondition(page: Page, condition: string) {
    let preTriage=new PreTriage(page)
    await preTriage.inputFieldCondition.type(condition)
    await preTriage.searchResults.first().waitFor()
    for(let i=0;i< await preTriage.searchResults.count();i++) {
        if(await preTriage.searchResults.nth(i).textContent()===condition) {
            await Promise.all([
                page.waitForNavigation(),
                preTriage.searchResults.nth(i).click(),
            ])  }} 
            await page.waitForTimeout(50)
     const count=await page.locator('button[data-testid="adult"]').count()      
        if (count===1) {
            await page.click('button[data-testid="adult"]') }
   else await preTriage.seekCareForAdult.click()
   
}

export async function seekCareForChildAndCondition(page: Page, condition: string) {
    let preTriage=new PreTriage(page)
    await preTriage.inputFieldCondition.type(condition)
    await preTriage.searchResults.first().waitFor()
    for(let i=0;i< await preTriage.searchResults.count();i++) {
        if(await preTriage.searchResults.nth(i).textContent()===condition) {
            await Promise.all([
                page.waitForNavigation(),
                preTriage.searchResults.nth(i).click(),
            ]) }}
            await page.waitForTimeout(50)
            const count=await page.locator('button[data-testid="adult"]').count()      
            if (count===1) {
            await page.click('button[data-testid="child"]')
          }
   else await preTriage.seekCareForChild.click()
}

export async function cancelTheDropInAppointment(page: Page) {
    let cancelAppointment=new CancelAppointment(page)
    await expect.soft(cancelAppointment.assertPlaceInQueue).toContainText(assert['waitingroom_place_in_queue'])
    await cancelAppointment.exitDropInQueue.click()
    await cancelAppointment.popUpSeeDetails.click()
    await cancelAppointment.cancelAppointmentButton.click()
    await cancelAppointment.yesCancelAppointmentBtn.click()
    await cancelAppointment.backToStartPageButton.click()
}

export async function ifPopUpShowThenCancelTheAppointment(page: Page) {
    let cancelAppointment=new CancelAppointment(page)
    await page.waitForLoadState('networkidle')
    const count= await cancelAppointment.popUpSeeDetails.count()
    if(count===1){
        await cancelAppointment.cancelTheAppointment()
    } }

export async function acceptTheConsent(page:Page) {
    let consent= new Consent(page)
    await expect.soft(consent.acceptConsentBtn).toBeDisabled()
    if (await consent.consentCheckBox.count()===1) {
        await consent.consentCheckBox.click()
    } else {
        await consent.consentYesRadioBtn.click()
    }
    await expect.soft(consent.acceptConsentBtn).toBeEnabled()
    await consent.acceptConsentBtn.click()
}  

export async function ifConsentForMedicalHistoryShowThenConsent(page:Page) {
    let consent= new Consent(page)
    await page.waitForLoadState('networkidle')
    if(await consent.acceptAccessMedicalRecords.count()===1){
        await expect.soft(consent.acceptConsentBtn).toBeDisabled()
        await consent.acceptAccessMedicalRecords.click()
        await expect.soft(consent.acceptConsentBtn).toBeEnabled()
        await Promise.all([ 
            page.waitForLoadState('networkidle'),
            consent.acceptConsentBtn.click() ])   }
    else if(await consent.acceptAccessPharmacyRecords.count()===1){
        await expect.soft(consent.acceptConsentBtn).toBeDisabled()
        await consent.acceptAccessPharmacyRecords.click()
        await expect.soft(consent.acceptConsentBtn).toBeEnabled()
        await Promise.all([ 
            page.waitForLoadState('networkidle'),
            consent.acceptConsentBtn.click() ])  }
        await page.waitForLoadState('networkidle')
    if(await consent.acceptAccessPharmacyRecords.count()===1){
        await expect.soft(consent.acceptConsentBtn).toBeDisabled()
        await consent.acceptAccessPharmacyRecords.click()
        await expect.soft(consent.acceptConsentBtn).toBeEnabled()
        await consent.acceptConsentBtn.click() }
    }

export async function seekcareforCondition(page: Page, condition: string) {
    let preTriage=new PreTriage(page)
    await preTriage.inputFieldCondition.type(condition)
    await preTriage.searchResults.first().waitFor()
    for(let i=0;i< await preTriage.searchResults.count();i++) {
        if(await preTriage.searchResults.nth(i).textContent()===condition) {
            await preTriage.searchResults.nth(i).click()
        }} 
}

export async function ifContactDetailFormShowThenHandleIt(page:Page, phone:string, email:string) {
    let myProfile= new Myprofile(page)
    await page.waitForLoadState('networkidle')
    const count= await myProfile.inputFieldEmail.count()
    if(count===1){
        const valuePhone= await myProfile.inputFieldMobile.getAttribute('value')    
        if(valuePhone===""){
            await myProfile.inputFieldMobile.fill(phone)
            await myProfile.inputFieldEmail.fill(email)
            await myProfile.saveContactDetailsBtn.click() } 
        else{
            await myProfile.saveContactDetailsBtn.click()
    }}}

export async function openingHours(start: number, end: number) {
    return Array(end - start + 1).fill().map((_, idx) => start + idx)  
}
    
