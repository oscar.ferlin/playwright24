import { devices, PlaywrightTestConfig } from "@playwright/test";

const config: PlaywrightTestConfig = {
   timeout:90000,
    retries: 0, 
    testDir:"testCases/partners",
    reporter:"html",
    workers: 3,
    use: {
        trace: 'retain-on-failure',
        headless: true,
        ...devices['Galaxy S9+'],
        //viewport: {width: 1280, height: 720},
        ignoreHTTPSErrors: true,
        actionTimeout: 10000,
        video:'retain-on-failure',
        screenshot:'only-on-failure',
        httpCredentials:{
            username:'platform24',
            password:'7592',
        },
    },
    expect: {
        timeout: 10000
      },/*
        projects: [ {
        name: 'chromium',
        use: {browserName:'chromium' }
    }, 
    {
        name: 'firefox',
        use: {browserName:'firefox' }
    }, 
    {
        name: 'webkit',
        use: {browserName:'webkit'  }
     
],}*/ 
}
export default config