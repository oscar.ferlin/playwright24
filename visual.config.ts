import { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
  timeout: 900000,
  retries: 0,
  testDir: 'testCases/visual',
  reporter: 'list',
  globalSetup: require.resolve('./testCases/loginCookies/logintoD24'),
  use: {
    storageState:'./testCases/loginCookies/loginCookiesD24.json',
    headless: true,
    //viewport: {width: 1280, height: 720},
    ignoreHTTPSErrors: true,
    actionTimeout: 10000,
    video: 'off',
    screenshot: 'off',
  },
  expect: {
    toMatchSnapshot: {
      maxDiffPixels: 20,
    },
  },
  
        projects: [ {
        name: 'chromium',
        use: {browserName:'chromium' }
    }, 
   /* {
        name: 'firefox',
        use: {browserName:'firefox' }
    }, 
    {
        name: 'webkit',
        use: {browserName:'webkit'  } */
     
],}
export default config;
