import { Page, request } from "@playwright/test";

export async function getLoginToken(requestURL:string, ID:string, xOrigin:string) {
   const requestContext= await request.newContext()
   const response= await requestContext.post(requestURL, {
       data:{
        'surname':'Lövgren',
        'givenName':'Sarah',
        'nationalPersonalId':ID,
        'personalIdType':'SWEDISH_PERSONAL_IDENTITY_NUMBER'
       },
       headers:{
        'x-origin': xOrigin } })
        const responseBody= JSON.parse(await response.text())
        let token=responseBody.token
        return token
}
export async function getResponse(requestURL:string, token:string, xOrigin:string) {
    const requestContext= await request.newContext()
    const response= await requestContext.get(requestURL, {
       headers:{
           'authorization':`Bearer ${token}`,
           'Content-Type': 'application/json',
           'x-origin': xOrigin } })
    const responseBody= JSON.parse(await response.text())
    return responseBody
}

export async function getAppointmentID(page:Page) {
    const currentURL= await page.url()
    const appointmentID= await currentURL.split('/')[4].trim()
    return appointmentID
}
