import { PlaywrightTestConfig } from "@playwright/test";

const config: PlaywrightTestConfig = {
    timeout:900000,
    retries: 0,
    testDir:"testCases/visual/frontDoor.spec.ts",
    reporter:'list',
    globalSetup: require.resolve('./testCases/loginCookies/logintoD24'),
    use: {
        storageState:'./testCases/loginCookies/loginCookiesD24.json',
        headless: true,
        //viewport: {width: 1280, height: 720},
        ignoreHTTPSErrors: true,
        actionTimeout: 15000,
        video:'off',
        screenshot:'off'
        //viewport:{width:1200, height:1200}
    },
    expect:{
        toMatchSnapshot:{
            maxDiffPixels:5
        }
    },
    /*
        projects: [ {
        name: 'chromium',
        use: {browserName:'chromium' }
    }, 
    {
        name: 'firefox',
        use: {browserName:'firefox' }
    }, 
    {
        name: 'webkit',
        use: {browserName:'webkit'  }
     
],}*/ 
}
export default config