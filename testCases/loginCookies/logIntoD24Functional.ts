import {chromium,FullConfig } from '@playwright/test';
import{HomePage} from '../../pageobjects/homepage/HomePage'
const data=JSON.parse(JSON.stringify(require("../../utils/dev/visualTest.json")))

async function globalSetup(config: FullConfig) {
    const browser = await chromium.launch();
    const page = await browser.newPage();
    let homePage= new HomePage(page)
    await page.goto(data['Doktor24-URL'])
    await homePage.acceptCookiesD24.click()
    await homePage.loginToPatientAppD24(data['functional-ID-D24'])
    await page.waitForLoadState('networkidle')
    // Save signed-in state to 'storageState.json'.
    await page.context().storageState({path:'./testCases/loginCookies/loginCookiesD24Functional.json'})
    await browser.close();
  }
  
  export default globalSetup;