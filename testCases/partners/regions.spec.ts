import{test, expect} from '@playwright/test'
import{HomePage} from "../../pageobjects/homepage/HomePage"
import {Exits} from "../../pageobjects/exit-payments/Exits";
import {Springworm} from"../../pageobjects/triage/Springworm"
import { CancelAppointment } from '../../pageobjects/homepage/CancelAppointment';
import {ifConsentForMedicalHistoryShowThenConsent, openingHours,ifPopUpShowThenCancelTheAppointment,seekCareForAdultAndCondition,seekCareForChildAndCondition, confirmContactDetails, cancelTheDropInAppointment, acceptTheConsent, seekcareforCondition,ifContactDetailFormShowThenHandleIt} from"../../commands"
import { Payments } from '../../pageobjects/exit-payments/Payments';
import { Wristinjuty } from '../../pageobjects/triage/Wristinjury';
import { Consent } from '../../pageobjects/homepage/Consent';
import {getAppointmentID, getLoginToken, getResponse} from'../../utils/api/response'
const data = JSON.parse(JSON.stringify(require("../../utils/dev/visualTest.json")))
const assert = JSON.parse(JSON.stringify(require("../../utils/assertions/assertionsSWE.json")))
const listingNorrbotten= JSON.parse(JSON.stringify(require("../../utils/dev/ListingNorrbotten.json")))

test.describe('Regions', ()=>{
    let homePage:HomePage
    let exits:Exits
    let springworm:Springworm
    let payment:Payments
    let wristinjury:Wristinjuty
    let consent:Consent
    let date:Date
    let cancelAppointment:CancelAppointment
    
test.beforeEach(async({ page }, testInfo) => {
    homePage=new HomePage(page)   
    exits=new Exits(page) 
    springworm=new Springworm(page)
    payment=new Payments(page)
    wristinjury=new Wristinjuty(page) 
    consent=new Consent(page)
    cancelAppointment=new CancelAppointment(page)
    date = new Date()
})

test('Region Västmanland', async ({page})=>{

    await page.goto(data['Region-Västmanland-URL'])
    await homePage.loginToPatientApp(data['ID3'])
    await ifContactDetailFormShowThenHandleIt(page, data['Phone'], data['Email'])
    await ifPopUpShowThenCancelTheAppointment(page)
    await homePage.menuItemSeekCareVästmanland.click() 
    await seekCareForAdultAndCondition(page, data['Wristinjury'])
    await wristinjury.navigateThroughAdultTriage()
    const openHours= await openingHours(9, 15)
    if(openHours.includes(date.getHours())){
        console.log("Executing Open Hours block",date.toLocaleDateString(),date.toLocaleTimeString())
        await exits.seekCareOnlineBtn.click()
        await page.waitForLoadState('networkidle');
        // page.waitForSelector('span[class*="WaitingRoomPosition"]', {state:'visible'})
        await cancelAppointment.assertPlaceInQueue.waitFor({state:'visible'})
        const appointmentID= await getAppointmentID(page)
        const token= await getLoginToken(`${data['Region-Västmanland-URL']}/api/test/login`, data['ID3'], 'regionvastmanland-1177')
        const responseBody= await getResponse(`${data['Region-Västmanland-URL']}/api/healthmanager/v1/appointments/${appointmentID}`, token, 'regionvastmanland-1177')
        expect.soft(responseBody.careUnitId).toBe(data['1177-RVM-CU-ID']);
        expect.soft(responseBody.priority).toBe(1);
        expect.soft(responseBody.practitionerRole).toBe('NURSE');
        await cancelTheDropInAppointment(page) }
    else{   
        console.log("Executing Closed Hours block",date.toLocaleDateString(), date.toLocaleTimeString())
        await expect(exits.elevenSeventySevenBtn).toBeVisible() } 
     })

})
