import{test, expect, devices, chromium} from '@playwright/test'
import { HomePage } from '../../pageobjects/homepage/HomePage';
import {PrescriptionRenewal} from "../../pageobjects/triage/PrescriptionRenewal"
import {ifContactDetailFormShowThenHandleIt,acceptTheConsent,seekcareforCondition,ifPopUpShowThenCancelTheAppointment,seekCareForAdultAndCondition,seekCareForChildAndCondition, confirmContactDetails, cancelTheDropInAppointment} from"../../commands"
import {Exits} from "../../pageobjects/exit-payments/Exits"
import {Springworm} from"../../pageobjects/triage/Springworm"
import {getLoginToken, getResponse, getAppointmentID} from "../../utils/api/response"
const data = JSON.parse(JSON.stringify(require("../../utils/dev/visualTest.json")))
const assert = JSON.parse(JSON.stringify(require("../../utils/assertions/assertionsSWE.json")))

test.describe('Insurances', () =>{
    let prescriptionRenewal:PrescriptionRenewal
    let homePage:HomePage
    let exits: Exits
    let springworm:Springworm

test.beforeEach(async({page})=>{
    prescriptionRenewal=new PrescriptionRenewal(page)
    homePage=new HomePage(page)
    exits= new Exits(page)
    springworm=new Springworm(page)
})   

test.fixme('Feelgood @CI', async ({page})=>{  
    
    await page.goto(data['Feelgood-URL'])
    await homePage.loginToPatientApp(data['Feelgood-ID'])
    await homePage.menuItemPrescriptionRenewal.click()
    await ifContactDetailFormShowThenHandleIt(page, data['Phone'], data['Email'])
    await ifPopUpShowThenCancelTheAppointment(page)
    await prescriptionRenewal.navigateThroughPrescriptionTriage()
    await exits.seekCareByDropIn()
    await prescriptionRenewal.navigateThroughPostTriageFinishInterviewer('Hello World')
    const appointmentID= await getAppointmentID(page)
    const token= await getLoginToken(`${data['Feelgood-URL']}/api/test/login`, data['Feelgood-ID'], 'feelgood')
    const responseBody= await getResponse(`${data['Feelgood-URL']}/api/healthmanager/v1/appointments/${appointmentID}`, token, 'feelgood')
    await exits.skipVideoTest.click()
    await expect(responseBody.careUnitId).toBe('doktor24_care_unit')
    await expect(responseBody.practitionerRole).toBe('DOCTOR')
    await expect(responseBody.priority).toBe(2)
    await cancelTheDropInAppointment(page)
})


test.only('SEB Auto-Refferal @CI', async ({page})=>{
    await page.goto(data['SEB-URL'])
    await homePage.loginToPatientApp(data['SEB-ID'])
    await acceptTheConsent(page)
    await ifContactDetailFormShowThenHandleIt(page, data['Phone'], data['Email'])
    await ifPopUpShowThenCancelTheAppointment(page)
    await homePage.menuItemSeekCare.click()
    await seekcareforCondition(page, data['Condition-AutoRefferal'])
    await springworm.getAutoRefferal()
    await exits.sendTheAutoRefferalButton.click()
    await page.waitForURL('**\/patientReferral')
    await expect(exits.patientRefferalTitle).toHaveText(assert['chat_exit_referral_sent_title'])
      
})


})