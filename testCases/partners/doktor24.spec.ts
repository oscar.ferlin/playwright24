import{test, expect} from '@playwright/test'
import{HomePage} from "../../pageobjects/homepage/HomePage"
import {Exits} from "../../pageobjects/exit-payments/Exits";
import{CancelAppointment} from "../../pageobjects/homepage/CancelAppointment"
import { FooterNavBar } from '../../pageobjects/components/FooterNavBar';
import{Payments} from "../../pageobjects/exit-payments/Payments"
import { Consent } from '../../pageobjects/homepage/Consent';
import {AcneDoktor24} from "../../pageobjects/triage/AcneDoktor24"
import {PreTriage} from "../../pageobjects/triage/Pre-triage"
import{Springworm} from "../../pageobjects/triage/Springworm"
import{confirmContactDetails,openingHours,ifContactDetailFormShowThenHandleIt,seekCareForAdultAndCondition, ifPopUpShowThenCancelTheAppointment, seekCareForChildAndCondition, cancelTheDropInAppointment}from "../../commands"
import { Psychology } from '../../pageobjects/triage/Psychology';
const data = JSON.parse(JSON.stringify(require("../../utils/dev/visualTest.json")))



test.describe('First Test Suite', ()=>{
    let homePage:HomePage; let cancelAppointment:CancelAppointment
    let exits:Exits
    let footerNavBar:FooterNavBar
    let payment:Payments
    let consent:Consent
    let acneDoktor24:AcneDoktor24
    let preTriage:PreTriage
    let psychology: Psychology
    let springWorm:Springworm
    let date: Date
    let openHours:number[]

    test.beforeEach(async({ page }, testInfo) => {
        
    homePage=new HomePage(page); exits=new Exits(page) 
    footerNavBar= new FooterNavBar(page)
    cancelAppointment= new CancelAppointment(page)
    payment= new Payments(page)
    consent= new Consent(page)
    acneDoktor24= new AcneDoktor24(page)
    preTriage= new PreTriage(page)
    psychology= new Psychology(page)
    date= new Date
    springWorm=new Springworm(page)
    openHours=await openingHours(7, 21)
    console.log(`Running ${testInfo.title}`)
});

    test('a DoctorFlow @CI', async({page}) =>{
        await page.goto(data['Doktor24-URL'])
        await homePage.acceptCookiesD24.click()
        await homePage.loginToPatientAppD24(data['ID-D24'])
        await ifContactDetailFormShowThenHandleIt(page, data['Phone'], data['Email'])
        await ifPopUpShowThenCancelTheAppointment(page)
        await homePage.menuItemSeekCare.click()
        await seekCareForAdultAndCondition(page, data['conditionDoctor'])
        await acneDoktor24.navigateThroughAcneD24Triage()
        await exits.seekCareByDropIn()      
        await payment.payWithKlarnaInvoice(data['billingEmail'], data['billingPostalCode'])
        //await consent.acceptTheConsentEscalation()
        await cancelAppointment.exitDropInQueue.click()
        await cancelAppointment.cancelTheAppointment()        
    })

    test('NurseFlow @CI', async ({page})=>{ 
        await page.goto(data['Doktor24-URL'])
        await homePage.acceptCookiesD24.click()
        await homePage.loginToPatientAppD24(data['ID-D24'])
        await ifContactDetailFormShowThenHandleIt(page, data['Phone'], data['Email'])
        await ifPopUpShowThenCancelTheAppointment(page)
        await homePage.menuItemSeekCare.click()
        await preTriage.seekCareForAdultAndCondition(data['conditionNurse'])
        if(openHours.includes(date.getHours())){
            console.log("Executing Open Hours block",date.toLocaleDateString(),date.toLocaleTimeString())
        }
        else {
            console.log('Executing Closed Hours block',date.toLocaleDateString(),date.toLocaleTimeString())
            await springWorm.navigateThroughTheTriage()
            await exits.dropInButton.click()
            await springWorm.navigateThroughPostTriage()
        }
       // await consent.acceptTheConsentEscalation()
        await cancelAppointment.exitDropInQueue.click(); await cancelAppointment.cancelTheAppointment()
    })

    test('Psychology', async ({page})=>{ 
        await page.goto(data['Doktor24-URL'])
        await homePage.acceptCookiesD24.click()
        await homePage.loginToPatientAppD24(data['ID-D24'])
        await ifContactDetailFormShowThenHandleIt(page, data['Phone'], data['Email'])
        await ifPopUpShowThenCancelTheAppointment(page)
        await homePage.menuItemPsychology.click()
        await preTriage.nextButtonPsychology.click()
        await preTriage.seekCareForAdult.click()
        await page.waitForLoadState('networkidle')
        await psychology.navigateThroughTriage(page)
        await exits.optOutExitBookAnAppointment()
        await payment.seekCareWithFreePass()
        await psychology.navigateThroughPostTriage()
     })

     test('Child flow Nurse @CI', async ({page})=>{
         await page.goto(data['Doktor24-URL'])
         await homePage.acceptCookiesD24.click()
         await homePage.loginToPatientAppD24(data['ID-D24'])
         await ifContactDetailFormShowThenHandleIt(page, data['Phone'], data['Email'])
         await ifPopUpShowThenCancelTheAppointment(page)
         await homePage.menuItemSeekCare.click()
         await seekCareForChildAndCondition(page, data['springworm'])
         if(openHours.includes(date.getHours())){
            console.log("Executing Open Hours block",date.toLocaleDateString(),date.toLocaleTimeString())
            await page.click('button:has-text("Baby Yoda")')
         }
         else {
            console.log("Executing Closed Hours block",date.toLocaleDateString(), date.toLocaleTimeString())
            await springWorm.navigateThroughChildTriage()
            await exits.dropInButton.click()
            await page.click('button:has-text("Baby Yoda")'),
            await confirmContactDetails(page)
            await springWorm.navigateThroughPostTriage()
            }
        // await consent.acceptTheConsentEscalation()   
         await cancelTheDropInAppointment(page)
         })
    })