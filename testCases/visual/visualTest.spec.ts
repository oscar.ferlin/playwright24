import{test, expect} from '@playwright/test'
import{HomePage} from '../../pageobjects/homepage/HomePage'
import { Myprofile } from '../../pageobjects/homepage/Myprofile'
import { Consent } from '../../pageobjects/homepage/Consent'
import{acceptTheConsent, ifContactDetailFormShowThenHandleIt, ifPopUpShowThenCancelTheAppointment}from "../../commands"
const dataset= JSON.parse(JSON.stringify(require("../../utils/staging/visualArrayIntergrationStaging.json")))
const data1= JSON.parse(JSON.stringify(require("../../utils/staging/visualArrayStaging.json")))
const data2= JSON.parse(JSON.stringify(require("../../utils/staging/visualArrayNoUpFrontLoginStaging.json")))
const data3=JSON.parse(JSON.stringify(require("../../utils/staging/visualTestStaging.json")))

test.describe("Visual Regression Testing", ()=> {
    test.slow
    let homePage:HomePage
    let myProfile:Myprofile
    let consent:Consent
    

    test.beforeEach(async ({page})=>{
        homePage=new HomePage(page)
        myProfile=new Myprofile(page)
        consent=new Consent(page)
        
    })

    for(const data of dataset){
        test(`Intergration ${data['Partner']}`, async ({page})=> {
            await page.goto(data['URL'])
            await homePage.loginPage.waitFor({state:'visible'})
            await page.waitForLoadState('networkidle')
            expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot(data['LoginPageIMG'])
            await page.goto(data['URL']+'/login/bankid')
            await homePage.inputFieldID.fill(data['ID'])
            await homePage.testLoginBtn.click()
            await ifContactDetailFormShowThenHandleIt(page, data3['Phone'], data3['Email'])
            expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot(data['LogoIMG'])
        })}

    for(const data of data1){
        test(`${data['Partner']}`, async ({page})=> {
            await page.goto(data['URL'])
            await homePage.loginPage.waitFor({state:'visible'})
            await page.waitForLoadState('networkidle')
            expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot(data['LoginPageIMG'])
            await homePage.loginToPatientApp(data['ID'])
            await ifContactDetailFormShowThenHandleIt(page, data3['Phone'], data3['Email'])
            expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot(data['LogoIMG'])
        })} 

    for(const data of data2){
        test(`${data['Partner']}`, async ({page})=> {
            await page.goto(data['URL'])
            await homePage.loginPage.waitFor({state:'visible'})
            await page.waitForLoadState('networkidle')
            if ((await page.$('#onetrust-accept-btn-handler')) !== null) {
            await homePage.acceptCookiesD24.click()
            await homePage.acceptCookiesD24.waitFor({state:'hidden'})
            await page.waitForLoadState('networkidle')
          }
            expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot(data['LoginPageIMG'], {threshold:0.002})
        })} 
        

})