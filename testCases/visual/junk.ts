/*
const data= JSON.parse(JSON.stringify(require("../../utils/visualTest.json")))

test("Aleris-Norway @intergration", async ({page})=> {
    await page.goto(data['Aleris-No-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Aleris-NO.png')
    await page.goto(data['Aleris-No-URL']+'/login/bankid')
    await homePage.inputFieldID.fill(data['Aleris-No-ID'])
    await homePage.testLoginBtn.click()
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Aleris-No.png')

})

test("Apoteket @noUpFrontLogin", async ({page})=> {
    await page.goto(data['Apoteket-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Apoteket.png')
})

test("Avonova-Norway", async ({page})=> {
    //fullPage
    await page.goto(data['Avonova-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Avonova-NO.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Avonova-NO.png')  
})

test("Acqua-DE", async ({page})=> {
    // fail
    await page.goto(data['Acqua-DE-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Acqua-DE.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Acqua-DE.png')  
})

test("Capio-DK @intergration", async ({page})=> {
    //fullPage
    await page.goto(data['Capio-DK-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Capio-DK.png')
    await page.goto(data['Capio-DK-URL']+'/login/bankid')
    await homePage.inputFieldID.fill(data['Capio-DK-ID'])
    await homePage.testLoginBtn.click()
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Capio-DK.png')  
})

test("Cereb", async ({page})=> {
    // fail
    await page.goto(data['Cereb-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Cereb.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Cereb.png')  
})

test("DKV @noUpFrontLogin", async ({page})=> {
    await page.goto(data['DKV-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-DKV.png')
})

test("Doktor24 @noUpFrontLogin", async ({page})=> {
    await page.goto(data['Doktor24-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await homePage.acceptCookiesD24.click()
    await page.waitForTimeout(2000)
    expect(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Doktor24.png')
})

test("Euroaccident @noUpFrontLogin", async ({page})=> {
    await page.goto(data['Euroaccident-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Euriaccident.png')
})

test("Familjeläkarna mitt", async ({page})=> {
    //fullPage
    await page.goto(data['Familjeläkarna-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Familjeläkarna.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Familjeläkarna.png')
})

test("Feelgood", async ({page})=> {
    //fullPage
    await page.goto(data['Feelgood-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Feelgood.png')
    await homePage.loginToPatientApp(data['Feelgood-ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Feelgood.png')
})

test.fixme("Consent dont show all the time need if condition if-se", async ({page})=> {
    //fullPage
    await page.goto(data['if-se-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-if-se.png')
    await homePage.loginToPatientApp(data['ID'])
    await consent.consentTextInfo.waitFor({state:'visible'})
    expect.soft(await consent.consentPage.screenshot()).toMatchSnapshot('Consent-if-se.png')
    await consent.consentCheckBox.check()
    await consent.acceptConsentBtn.click()
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-if-se.png')
})

test("Länsförsäkringar @consent", async ({page})=> {
    //fullPage
    await page.goto(data['Länsförsäkringar-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Länsförsäkringar-se.png')
    await homePage.loginToPatientApp(data['Länsförsäkringar-ID'])
    await consent.consentTextInfo.waitFor({state:'visible'})
    expect.soft(await consent.consentPage.screenshot()).toMatchSnapshot('Consent-Länsförsäkringar-se.png')
    await consent.consentCheckBox.check()
    await consent.acceptConsentBtn.click()
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Länsförsäkringar-se.png')
})

test("Region Halland", async ({page})=> {
    //fail cuz wrong on dev
    await page.goto(data['Region-Halland-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Region-Halland.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Region-Halland.png')
})

test("Region Gävleborg-DVM @intergration", async ({page})=> {
    //fullPage
    await page.goto(data['Gävleborg-DVM-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Region-Gavleborg-DVM.png')
    await page.goto(data['Gävleborg-DVM-URL']+'/login/bankid')
    await homePage.inputFieldID.fill(data['Gävleborg-DVM-ID'])
    await homePage.testLoginBtn.click()
    await myProfile.saveContactDetailsBtn.click()
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Region-Gavleborg-DVM.png')  
})

test("Region JH", async ({page})=> {
    //fullPage
    await page.goto(data['Region-JH-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Region-JH.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Region-JH.png')
})

test("Region Norrbotten @intergration", async ({page})=> {
    //fullPage
    await page.goto(data['Region-Norrbotten-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000) 
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Region-Norrbotten.png')
    await page.goto(data['Region-Norrbotten-URL']+'/login/bankid')
    await homePage.inputFieldID.fill(data['ID'])
    await homePage.testLoginBtn.click()
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Region-Norrbotten.png')
})

test("Region Skåne", async ({page})=> {
    //fullPage
    await page.goto(data['Region-Skåne-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Region-Skåne.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Region-Skåne.png')
})

test("Region Sörmland @intergration", async ({page})=> {
    //fullPage
    await page.goto(data['Region-Sörmland-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Region-Sörmland.png')
    await page.goto(data['Region-Sörmland-URL']+'/login/bankid')
    await homePage.inputFieldID.fill(data['ID'])
    await homePage.testLoginBtn.click()
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Region-Sörmland.png')
})

test("Region Västerbotten", async ({page})=> {
    //fullPage
    await page.goto(data['Region-Västerbotten-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Region-Västerbotten.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Region-Västerbotten.png')
})

test("Region Västernorrland", async ({page})=> {
    //fullPage
    await page.goto(data['Region-Västernorrland-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Region-Västernorrland.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Region-Västernorrland.png')
})

test("Region Västmanland", async ({page})=> {
    //fullPage
    await page.goto(data['Region-Västmanland-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Region-Västmanland.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Region-Västmanland.png')
})

test("SEB @consent", async ({page})=> {
    //fullPage
    await page.goto(data['SEB-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    await page.waitForTimeout(2000)
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-SEB.png')
    await homePage.loginToPatientApp(data['SEB-ID'])
    await consent.consentTextInfo.waitFor({state:'visible'})
    expect.soft(await consent.consentPage.screenshot()).toMatchSnapshot('Consent-SEB.png')
    await consent.consentCheckBox.check()
    await consent.acceptConsentBtn.click()
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-SEB.png')
})

test("Skandia", async ({page})=> {
    //fullPage
    await page.goto(data['Skandia-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Skandia.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Skandia.png')
})

test("Stuvsta-vc", async ({page})=> {
    //fullPage
    await page.goto(data['Stuvsta-vc-URL'])
    await homePage.loginPage.waitFor({state:'visible'})
    expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('loginPage-Stuvsta-vc.png')
    await homePage.loginToPatientApp(data['ID'])
    expect.soft(await homePage.logoBanner.screenshot()).toMatchSnapshot('logo-Stuvsta-vc.png')
})*/