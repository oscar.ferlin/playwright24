import{test, expect} from '@playwright/test'
import{HomePage} from '../../pageobjects/homepage/HomePage'
import { Myprofile } from '../../pageobjects/homepage/Myprofile'
import { PreTriage } from '../../pageobjects/triage/Pre-triage'
import { FooterNavBar } from '../../pageobjects/components/FooterNavBar'
import{acceptTheConsent, ifContactDetailFormShowThenHandleIt, ifPopUpShowThenCancelTheAppointment}from "../../commands"
const data=JSON.parse(JSON.stringify(require("../../utils/dev/visualTest.json")))

test.describe("Front Door specific", ()=> {
    test.slow
    let homePage:HomePage
    let myProfile:Myprofile
    let preTriage:PreTriage
    let footerNavBar:FooterNavBar


    test.beforeEach(async ({page})=> {
       homePage=new HomePage(page)
       footerNavBar=new FooterNavBar(page)
       preTriage= new PreTriage(page)
     
    })

    test('ask_For_Patient', async ({page})=>{
        await page.goto(`${data['Doktor24-URL']}/askForPatient?redirectTo=%2Fsearch%2Fcough%3FsearchQuery%3D&closable=true`)
        await page.waitForLoadState('networkidle')
        expect(await preTriage.askForPatientVisualTest.screenshot({mask:[preTriage.maskOneSelfElementAskForPatient]})).toMatchSnapshot('askForPatient-SWE.png')
    })

    test('Intro page Psychology', async ({page})=> {
        await page.goto(`${data['Doktor24-URL']}/meetPsychologist`)
        await page.waitForLoadState('networkidle')
        expect(await homePage.loginPage.screenshot()).toMatchSnapshot('Intro-page-Psychology-SWE.png')
    })

    test('Intro page Prescription Renewal', async ({page})=> {
        await page.goto(`${data['Doktor24-URL']}/prescriptionRenewal`)
        await page.waitForLoadState('networkidle')
        expect(await homePage.loginPage.screenshot()).toMatchSnapshot('Intro-page-PrescriptionRenewal.png')
    })

    test('My Profile', async ({page})=> {
        //add page locator to myProfile later
        await page.goto(`${data['Doktor24-URL']}/aboutyou`)
        await page.waitForLoadState('networkidle')
        expect.soft(await page.locator('div[class*="Tabs"]').screenshot({mask:[page.locator('div[class*="PatientTitle"]')]})).toMatchSnapshot('myProfile.png')
        expect.soft(await page.locator('nav button').screenshot()).toMatchSnapshot('Logout-button.png')
    })

    test('/aboutyou/myChildren page', async ({page})=> {
        await page.goto(`${data['Doktor24-URL']}/aboutyou/myChildren`)
        await page.waitForLoadState('networkidle')
        expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('Aboutyou-child-page.png')
        await page.locator('a:has-text("Baby Yoda")').click()
        expect.soft(await homePage.loginPage.screenshot()).toMatchSnapshot('Child-profile-page.png')
    })

    test('/addChild page', async ({page})=> {
        await page.goto(`${data['Doktor24-URL']}/addChild`)
        await page.waitForLoadState('networkidle')
        expect(await homePage.loginPage.screenshot()).toMatchSnapshot('Add-child-page.png')
    })

    test('/aboutyou/editChildProfile', async ({page})=> {
        await page.goto(`${data['Doktor24-URL']}/aboutyou/editChildProfile/e29a1507-d552-46c1-8ae9-ff756eabc686`)
        await page.waitForLoadState('networkidle')
        expect(await homePage.loginPage.screenshot()).toMatchSnapshot('edit-child-myProfile.png')
    })

    test('/contactDetails page', async ({page})=> {
        await page.goto(`${data['Doktor24-URL']}/contactDetails?closable=true`)
        await page.waitForLoadState('networkidle')
        expect(await homePage.loginPage.screenshot()).toMatchSnapshot('Contact-Details.png')
    })

    test('Support page', async ({page})=> {
        await page.goto(`${data['Doktor24-URL']}/support`)
        await page.waitForLoadState('networkidle')
        expect(await homePage.loginPage.screenshot()).toMatchSnapshot('Support.png')
    })


})  