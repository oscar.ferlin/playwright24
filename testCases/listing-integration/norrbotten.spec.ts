import{test, expect} from '@playwright/test'
import{HomePage} from "../../pageobjects/homepage/HomePage"
import {Exits} from "../../pageobjects/exit-payments/Exits";
import {Springworm} from"../../pageobjects/triage/Springworm"
import { Wristinjuty } from '../../pageobjects/triage/Wristinjury';
import {seekCareForAdultAndConditionNewPatientModel,ifConsentForMedicalHistoryShowThenConsent, openingHours,ifPopUpShowThenCancelTheAppointment,seekCareForAdultAndCondition,seekCareForChildAndCondition, confirmContactDetails, cancelTheDropInAppointment, acceptTheConsent, seekcareforCondition,ifContactDetailFormShowThenHandleIt} from"../../commands"
import { Payments } from '../../pageobjects/exit-payments/Payments';
import {getAppointmentID, getLoginToken, getResponse} from'../../utils/api/response'
const data = JSON.parse(JSON.stringify(require("../../utils/dev/visualTest.json")))
const assert = JSON.parse(JSON.stringify(require("../../utils/assertions/assertionsSWE.json")))
const listingNorrbotten= JSON.parse(JSON.stringify(require("../../utils/dev/ListingNorrbotten.json")))
const fridayToSunday=[5,6,0]

test.describe('Listing-Intergration', ()=>{
    let homePage:HomePage
    let exits:Exits
    let springworm:Springworm
    let payment:Payments
    let date:Date
    let wristinjury:Wristinjuty
    let listingHour1:number[]
    let listingHour2:number[]
    
test.beforeEach(async({ page }, testInfo) => {
    homePage=new HomePage(page)   
    exits=new Exits(page) 
    springworm=new Springworm(page)
    payment=new Payments(page)
    wristinjury=new Wristinjuty(page)
    date = new Date() 
    listingHour1= await openingHours(8,11)
    listingHour2= await openingHours(12,15)
    
})

for(const listed of listingNorrbotten){
    test(`Region Norrbotten prio 3-4 ${listed['User']}`, async({page}) =>{
        await page.goto(data['Region-Norrbotten-URL']+'/login/bankid')
        await homePage.inputFieldID.fill(listed['ID'])
        await homePage.testLoginBtn.click()
        await ifContactDetailFormShowThenHandleIt(page, data['Phone'], data['Email'])
        await ifPopUpShowThenCancelTheAppointment(page)
        await homePage.menuItemSeekCareNorrBotten.click()
        await seekCareForAdultAndConditionNewPatientModel(page, data['springworm'])
        await springworm.navigateThroughTheTriage()
        await exits.seekCareByDropIn()
        await springworm.navigateThroughThePostTriageFinishInterview()
        await payment.offlinePayment.click()
        //await consent.acceptTheConsentEscalation()
        await ifConsentForMedicalHistoryShowThenConsent(page)
        const appointmentID= await getAppointmentID(page)
        const token= await getLoginToken(`${data['Region-Norrbotten-URL']}/api/test/login`, listed['ID'], 'regionnorrbotten-digitalen')
        const responseBody=await getResponse(`${data['Region-Norrbotten-URL']}/api/healthmanager/v1/appointments/${appointmentID}`, token, 'regionnorrbotten-digitalen')
    
       if (fridayToSunday.includes(date.getDay())) {
        console.log('Executing Weekend block',date.toLocaleDateString(),date.toLocaleTimeString())
        await expect.soft(responseBody.careUnitId).toBe(listed['Digitalen-CU'])
        await expect.soft(responseBody.priority).toBe(4)
        await expect.soft(responseBody.practitionerRole).toBe('NURSE')
       }
        else if (listingHour1.includes(date.getHours())) {
            console.log("Executing Open Hours 8:00-12:00 block",date.toLocaleDateString(),date.toLocaleTimeString())
            await expect.soft(responseBody.careUnitId).toBe(listed['Listed-CU'])
            await expect.soft(responseBody.priority).toBe(4)
            await expect.soft(responseBody.practitionerRole).toBe('NURSE')
        } 
        else if (listingHour2.includes(date.getHours())){
            console.log("Executing Open Hours 12:00-15:00 block",date.toLocaleDateString(),date.toLocaleTimeString())
            await expect.soft(responseBody.careUnitId).toBe(listed['Digitalen-CU'])
            await expect.soft(responseBody.priority).toBe(4)
            await expect.soft(responseBody.practitionerRole).toBe('NURSE')
        }
        else {
            console.log("Executing Closed Hours block 15:00-08:00",date.toLocaleDateString(),date.toLocaleTimeString())
            await expect.soft(responseBody.careUnitId).toBe(listed['Listed-CU'])
            await expect.soft(responseBody.priority).toBe(4)
            await expect.soft(responseBody.practitionerRole).toBe('NURSE')
        }
        console.log(responseBody)
        await cancelTheDropInAppointment(page)
    })}


    for(const listed of listingNorrbotten){
    test(`Region Norrbotten listing prio 1-2 ${listed['User']}`, async ({page}) =>{
    
        await page.goto(data['Region-Norrbotten-URL']+'/login/bankid')
        await homePage.inputFieldID.fill(listed['ID'])
        await homePage.testLoginBtn.click()
        await homePage.menuItemSeekCareNorrBotten.click()
        await seekCareForAdultAndConditionNewPatientModel(page, data['Wristinjury'])
        await wristinjury.navigateThroughAdultTriage()
        if(fridayToSunday.includes(date.getDay())) {
            console.log('Executing Weekend block',date.toLocaleDateString(),date.toLocaleTimeString())
            await expect(exits.elevenSeventySevenBtn).toBeVisible()
            await expect(exits.elevenSeventySevenBtn).toHaveText(assert['1177_exit_button'])
        } 
        else {
            await exits.dropInButton.click()    
            const appointmentID= await getAppointmentID(page)
            const token= await getLoginToken(`${data['Region-Norrbotten-URL']}/api/test/login`, listed['ID'], 'regionnorrbotten-digitalen')
            const responseBody= await getResponse(`${data['Region-Norrbotten-URL']}/api/healthmanager/v1/appointments/${appointmentID}`, token, 'regionnorrbotten-digitalen')

        if (listingHour1.includes(date.getHours())) {
            console.log("Executing Open Hours 8:00-12:00 block",date.toLocaleDateString(),date.toLocaleTimeString())
            await expect.soft(responseBody.careUnitId).toBe(listed['Listed-CU'])
            await expect.soft(responseBody.priority).toBe(1)
            await expect.soft(responseBody.practitionerRole).toBe('NURSE')
        }
        else if (listingHour2.includes(date.getHours())) {
            console.log("Executing Open Hours 12:00-15:00 block",date.toLocaleDateString(),date.toLocaleTimeString())
            await expect.soft(responseBody.careUnitId).toBe(listed['Digitalen-CU'])
            await expect.soft(responseBody.priority).toBe(1)
            await expect.soft(responseBody.practitionerRole).toBe('NURSE')
        }
        else {
            console.log("Executing Closed Hours block 15:00-08:00",date.toLocaleDateString(),date.toLocaleTimeString())
            //not sure on the config here.
            await expect(exits.elevenSeventySevenBtn).toBeVisible()
            await expect(exits.elevenSeventySevenBtn).toHaveText(assert['1177_exit_button'])
        } }
})}
    for(const listed of listingNorrbotten){
    test(`Region Norrbotten listing prio 5 ${listed['User']}`, async ({page})=>{
        await page.goto(data['Region-Norrbotten-URL']+'/login/bankid')
        await homePage.inputFieldID.fill(listed['ID'])
        await homePage.testLoginBtn.click()
        await ifContactDetailFormShowThenHandleIt(page, data['Phone'], data['Email'])
        await ifPopUpShowThenCancelTheAppointment(page)
        await homePage.menuItemSeekCareNorrBotten.click()
        await seekCareForAdultAndConditionNewPatientModel(page, data['springworm'])
        await springworm.getPrio5(); await exits.seekCareOnlineBtn.click()
        await springworm.navigateThroughThePostTriageFinishInterview()
        await payment.offlinePayment.click()
        console.log(date.toLocaleDateString(), date.toLocaleTimeString())
        const appointmentID= await getAppointmentID(page)
        const token= await getLoginToken(`${data['Region-Norrbotten-URL']}/api/test/login`, listed['ID'], 'regionnorrbotten-digitalen')
        const responseBody= await getResponse(`${data['Region-Norrbotten-URL']}/api/healthmanager/v1/appointments/${appointmentID}`, token, 'regionnorrbotten-digitalen')
        await expect.soft(responseBody.careUnitId).toBe(listed['Digitalen-CU'])
        await expect.soft(responseBody.priority).toBe(5)
        await expect.soft(responseBody.practitionerRole).toBe('NURSE')
        await cancelTheDropInAppointment(page)
    })}

})