import{test, expect} from '@playwright/test'
import{PickOrigin} from "../../pageobjects/homepage/PickOrigin"
import{HomePage} from "../../pageobjects/homepage/HomePage"
import {Exits} from "../../pageobjects/exit-payments/Exits";
import {Springworm} from"../../pageobjects/triage/Springworm"
import { Wristinjuty } from '../../pageobjects/triage/Wristinjury';
import {ifConsentForMedicalHistoryShowThenConsent, openingHours,ifPopUpShowThenCancelTheAppointment,seekCareForAdultAndCondition,seekCareForChildAndCondition, confirmContactDetails, cancelTheDropInAppointment, acceptTheConsent, seekcareforCondition,ifContactDetailFormShowThenHandleIt} from"../../commands"
import { Payments } from '../../pageobjects/exit-payments/Payments';
import {getAppointmentID, getLoginToken, getResponse} from'../../utils/api/response'
const data = JSON.parse(JSON.stringify(require("../../utils/staging/visualTestStaging.json")))
const assert = JSON.parse(JSON.stringify(require("../../utils/assertions/assertionsSWE.json")))
const listingSormland= JSON.parse(JSON.stringify(require("../../utils/staging/ListingSormland.json")))
const weekEnd=[6,0]

test.describe('Listing-Intergration', ()=>{
    let homePage:HomePage;
    let pickOrigin:PickOrigin;
    let exits:Exits
    let springworm:Springworm
    let payment:Payments
    let date:Date
    let wristinjury:Wristinjuty
    let listingHour1:number[]
    let listingHour2:number[]
    
test.beforeEach(async({ page }, testInfo) => {
    homePage=new HomePage(page)  
    exits=new Exits(page) 
    springworm=new Springworm(page)
    payment=new Payments(page)
    wristinjury=new Wristinjuty(page)
    pickOrigin= new PickOrigin(page)
    date = new Date() 
    listingHour1= await openingHours(8,15)
    listingHour2= await openingHours(16,17)
    
})

for(const listed of listingSormland){
test.skip(`Prio 1-3 ${listed['User']}`, async ({page})=>{
    await page.goto(`${data['Region-Sörmland-URL']}/login/bankid`)
    await homePage.inputFieldID.fill(listed['ID']); await Promise.all([
    page.waitForNavigation(),
    homePage.testLoginBtn.click() ])
    await pickOrigin.origins.first().click()
    await ifContactDetailFormShowThenHandleIt(page, data['Phone'], data['Email'])
    await ifPopUpShowThenCancelTheAppointment(page)
    await homePage.menuItemSeekCareSörmland.click()
    await seekCareForAdultAndCondition(page, data['Wristinjury'])
    await wristinjury.navigateThroughAdultTriage()
    await exits.seekCareOnlineBtn.click(); await payment.offlinePayment.click()
    const appointmentID= await getAppointmentID(page)
    const token= await getLoginToken(`${data['Region-Sörmland-URL']}/api/test/login`, listed['ID'], 'regionsormland')
    const responseBody= await getResponse(`${data['Region-Sörmland-URL']}/api/healthmanager/v1/appointments/${appointmentID}`, token, 'regionsormland')
    if(weekEnd.includes(date.getDay())){
        console.log('Executing the weekend block', date.toLocaleDateString(),date.toLocaleTimeString())
        await expect.soft(responseBody.careUnitId).toBe(listed['Central-CU'])
        await expect.soft(responseBody.priority).toBe(1)
        await expect.soft(responseBody.practitionerRole).toBe('NURSE') }
    else {
    if(listingHour1.includes(date.getHours())){
        console.log('Executing open hours 08:00-16:00', date.toLocaleDateString(),date.toLocaleTimeString())
        await expect.soft(responseBody.careUnitId).toBe(listed['Listed-CU'])
        await expect.soft(responseBody.priority).toBe(1)
        await expect.soft(responseBody.practitionerRole).toBe('NURSE')
    }
    else if(listingHour2.includes(date.getHours())){
        console.log('Executing open hours 16:00-18:00', date.toLocaleDateString(),date.toLocaleTimeString())
        await expect.soft(responseBody.careUnitId).toBe(listed['Central-CU'])
        await expect.soft(responseBody.priority).toBe(1)
        await expect.soft(responseBody.practitionerRole).toBe('NURSE') }
    else {
        console.log('Executing closed hours 18:00-08:00', date.toLocaleDateString(),date.toLocaleTimeString())
        await expect.soft(responseBody.careUnitId).toBe(listed['Central-CU'])
        await expect.soft(responseBody.priority).toBe(1)
        await expect.soft(responseBody.practitionerRole).toBe('NURSE') }
    }
    await cancelTheDropInAppointment(page)
})}


})