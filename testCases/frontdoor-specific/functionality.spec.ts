import {test, expect} from "@playwright/test"
import{HomePage} from '../../pageobjects/homepage/HomePage'
import { Myprofile } from '../../pageobjects/homepage/Myprofile'
import { HealthProfile } from "../../pageobjects/homepage/healthProfile"
import{acceptTheConsent, ifContactDetailFormShowThenHandleIt, ifPopUpShowThenCancelTheAppointment}from "../../commands"
const data=JSON.parse(JSON.stringify(require("../../utils/dev/visualTest.json")))

test.describe('Areas That Front Door Owns', ()=>{
    let homePage:HomePage
    let myProfile:Myprofile
    let healthProfile:HealthProfile

    test.beforeEach(async ({page})=> {
     homePage=new HomePage(page)
     healthProfile=new HealthProfile(page)
     myProfile= new Myprofile(page) })

test('/contactDetails', async ({page}) =>{
    await page.goto(`${data['Doktor24-URL']}/aboutyou`);
    await myProfile.about_label_contact.click()
    expect.soft(await myProfile.inputFieldMobile.inputValue()).toBeTruthy()
    expect.soft(await myProfile.inputFieldEmail.inputValue()).toBeTruthy()
    await myProfile.inputFieldMobile.selectText()
    await page.keyboard.press('Backspace')
    await myProfile.inputFieldEmail.selectText()
    await page.keyboard.press('Backspace')
    await expect.soft(myProfile.saveContactDetailsBtn).toBeDisabled()
    await myProfile.inputFieldMobile.fill(data['Phone'])
    await myProfile.inputFieldEmail.fill(data['Email'])
    await myProfile.saveContactDetailsBtn.click()
    await expect.soft(myProfile.patientName).toBeVisible()
    await myProfile.about_label_contact.click()
    expect.soft(await myProfile.inputFieldMobile.inputValue()).toBe(data['Phone'])
    expect.soft(await myProfile.inputFieldEmail.inputValue()).toBe(data['Email'])
    await myProfile.backButton.click()
    await expect.soft(myProfile.patientName).toBeVisible() })


test('/healthData?child', async ({page}) =>{
    await page.goto(`${data['Doktor24-URL']}/aboutyou/childProfile/892449ad-fee0-48b3-a2d7-548a1ef4a2df`);
    await page.waitForLoadState('networkidle'); 
    await Promise.all([
        myProfile.about_you_health_profile.click(),
        page.waitForNavigation(), ])
    await healthProfile.clearHealthProfileData()
    await healthProfile.fillInTheHealthData('6','64','a','b','c','d')
    await myProfile.backButton.click()
    await page.goto(`${data['Doktor24-URL']}/healthData?childId=892449ad-fee0-48b3-a2d7-548a1ef4a2df`);
    await page.waitForLoadState('networkidle')
    expect.soft(await healthProfile.weight.inputValue()).toBe('6')
    expect.soft(await healthProfile.height.inputValue()).toBe('64')
    expect.soft(await healthProfile.allergies.inputValue()).toBe('a')
    expect.soft(await healthProfile.diagnoses.inputValue()).toBe('b')
    expect.soft(await healthProfile.surgeries.inputValue()).toBe('c')
    expect.soft(await healthProfile.medication.inputValue()).toBe('d') })

test('/myVaccine', async ({page}) =>{  
    await page.goto(`${data['Doktor24-URL']}/aboutyou`)
    await myProfile.my_vaccinations.click()
    const [newTab]= await Promise.all([
        page.waitForEvent('popup'),
        myProfile.vaccination_cta_button.click() ])
    await expect(newTab).toHaveURL('https://doktor24.se/vaccin/');  
}) 
})

