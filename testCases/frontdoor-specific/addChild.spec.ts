import {test, expect} from "@playwright/test"
import{HomePage} from '../../pageobjects/homepage/HomePage'
import { Myprofile } from '../../pageobjects/homepage/Myprofile'
import { AddChild } from "../../pageobjects/homepage/AddChild"
const data=JSON.parse(JSON.stringify(require("../../utils/dev/visualTest.json")))

test.describe.serial('Areas That Front Door Owns', ()=>{
    let homePage:HomePage
    let myProfile:Myprofile
    let addChild:AddChild


    test.beforeEach(async ({page})=> {
     homePage=new HomePage(page)
     addChild=new AddChild(page)
     myProfile= new Myprofile(page) })
    

     test('/addChild', async ({page}) =>{  
        await page.goto(`${data['Doktor24-URL']}/addChild`)
        await addChild.addChild('Harry','Potter',data['add-child-ID'])
    })

    test('/addChildAgain', async ({page}) =>{  
        await page.goto(`${data['Doktor24-URL']}/addChild`)
        await addChild.addChild('Harry','Potter',data['add-child-ID'])
        await addChild.childAlreadyExistErrorMessage.isVisible()
    })

    test('/deleteChild', async ({page}) =>{
        await page.goto(`${data['Doktor24-URL']}/aboutyou/myChildren`)
        await page.click('a:has-text("Harry Potter")')
        await addChild.deleteTheChildProfile()
        await page.waitForLoadState('networkidle')
        expect(await addChild.countOfChildren.count()).toBe(1)
    })
    
    
    
    
    })