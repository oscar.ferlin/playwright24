import { expect, Locator, Page, Selectors } from "@playwright/test";
export class Myprofile{
    //Define selectors
    readonly page:Page
    readonly saveContactDetailsBtn: Locator
    readonly inputFieldMobile:Locator
    readonly inputFieldEmail:Locator   
    readonly about_label_contact:Locator
    readonly patientName:Locator
    readonly backButton:Locator
    readonly about_you_health_profile:Locator
    readonly my_vaccinations:Locator
    readonly vaccination_cta_button:Locator


    //init selectors with constructor
    constructor(page:Page) {
        this.page=page
        this.saveContactDetailsBtn=page.locator('button[data-testid="select-time-slot-confirm-button"]')
        this.inputFieldMobile=page.locator('input[data-testid="contact-details-mobile-number-input"]')
        this.inputFieldEmail=page.locator('input[data-testid="contact-details-email-input"]')
        this.about_label_contact=page.locator('a[href*="/contactDetails"]')
        this.patientName=page.locator('h1[data-testid="name"]')
        this.backButton=page.locator('div[class*="navigation-bar"] button')
        this.about_you_health_profile=page.locator('a[href*="/healthData"]')
        this.my_vaccinations=page.locator('a[href="/myVaccine"]')
        this.vaccination_cta_button=page.locator('a[href="https://doktor24.se/vaccin/"]')
    }

}
