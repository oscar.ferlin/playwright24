import { expect, Locator, Page, Selectors } from "@playwright/test";
import { Myprofile } from "./Myprofile";
export class AddChild extends Myprofile{
    readonly page:Page
    readonly child_name_placeholder:Locator
    readonly child_surname_placeholder:Locator
    readonly child_id_placeholder:Locator
    readonly child_add_button:Locator
    readonly edit_profile:Locator
    readonly remove_child_profile:Locator
    readonly yes:Locator
    readonly countOfChildren:Locator
    readonly childAlreadyExistErrorMessage:Locator

    constructor(page:Page){
        super(page);
        this.child_name_placeholder=page.locator('input[data-testid="given-name"]')
        this.child_surname_placeholder=page.locator('input[data-testid="surname"]')
        this.child_add_button=page.locator('button[data-testid="add-child-button"]')
        this.child_id_placeholder=page.locator('input[data-testid="personal-number-input"]')
        this.edit_profile=page.locator('button p')
        this.remove_child_profile=page.locator('button[role="link"]')
        this.yes=page.locator('button[data-testid*="modal"]:nth-child(2) >> visible=true')
        this.countOfChildren=page.locator('a[href*="/aboutyou/childProfile/"]')
        this.childAlreadyExistErrorMessage=page.locator('span:has-text("Child already exists")')
    }

    async addChild(firstName:string, surName:string, ID:string){
        await this.child_add_button.isDisabled()
        await this.child_name_placeholder.fill(firstName)
        await this.child_surname_placeholder.fill(surName)
        await this.child_id_placeholder.fill(ID)
        await this.child_add_button.isEnabled()
        await this.child_add_button.click()
    }

    async deleteTheChildProfile(){
        await this.edit_profile.click()
        await this.remove_child_profile.click()
        await this.yes.click()
    }
}