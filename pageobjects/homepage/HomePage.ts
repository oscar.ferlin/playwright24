import { expect, Locator, Page, Selectors } from "@playwright/test";
import { PreTriage } from "../triage/Pre-triage";
export class HomePage{
    //Define selectors
    readonly page:Page
    readonly loginButton: Locator
    readonly inputFieldID: Locator
    readonly testLoginBtn: Locator
    readonly menuItemPrescriptionRenewal: Locator
    readonly doktor24LoginBtn: Locator
    readonly acceptCookiesD24: Locator
    readonly menuItemSeekCare: Locator
    readonly menuItemPsychology: Locator
    readonly loginPage: Locator
    readonly logoBanner: Locator
    readonly menuItemSeekCareNorrBotten: Locator
    readonly menuItemSeekCareVästmanland: Locator
    readonly menuItemSeekCareSörmland: Locator


    //init selectors with constructor
    constructor(page:Page) {
        this.page=page
        this.loginButton=page.locator('button[data-testid="login-button"]')
        this.inputFieldID=page.locator('input[data-testid="personal-number-input"]')
        this.testLoginBtn=page.locator('button[data-testid="test-login-button"]')
        this.menuItemPrescriptionRenewal=page.locator('button[data-testid*="prescriptionRenewal"]')
        this.doktor24LoginBtn=page.locator('a[data-testid="home-login-button"]')  
        this.acceptCookiesD24=page.locator('#onetrust-accept-btn-handler')
        this.menuItemSeekCare=page.locator('button[data-testid*="searchForCareOnline"]')
        this.menuItemPsychology=page.locator('button[data-testid*="meetPsychologist"]')
        this.loginPage=page.locator('div[class*="styles_container"]')
        this.logoBanner=page.locator('header[class*="LogoHeaderstyles"]')
        this.menuItemSeekCareNorrBotten=page.locator('button[data-testid="45b590db-bf84-489f-bffc-c06e217d5507"]')
        this.menuItemSeekCareVästmanland=page.locator('button[data-testid="ffde5277-eed5-499c-8e73-d97be4c1173b"]')
        this.menuItemSeekCareSörmland=page.locator('button[data-testid="7f053314-f60d-4a46-a633-6922e8b26ad7"]')
    }

    //define page methods
    async loginToPatientApp(ID:string) {
        await this.loginButton.click()
        await this.inputFieldID.type(ID)
        await this.testLoginBtn.click() }

    async loginToPatientAppD24(ID:string) {
        await this.doktor24LoginBtn.click()
        await this.inputFieldID.fill(ID)
        await this.testLoginBtn.click()
    }
    
 }
