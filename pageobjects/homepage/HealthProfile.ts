import { expect, Locator, Page} from "@playwright/test";
export class HealthProfile {
    readonly page: Page;
    readonly weight:Locator
    readonly height:Locator
    readonly allergies:Locator
    readonly diagnoses:Locator
    readonly surgeries:Locator
    readonly medication:Locator
    readonly saveButton:Locator

    constructor(page:Page){
        this.page=page;
        this.weight=page.locator('[data-testid="healthProfile__weight"]')
        this.height=page.locator('[data-testid="healthProfile__height"]')
        this.allergies=page.locator('[data-testid="healthProfile__allergies"]')
        this.diagnoses=page.locator('[data-testid="healthProfile__diagnoses"]')
        this.surgeries=page.locator('[data-testid="healthProfile__surgeries"]')
        this.medication=page.locator('[data-testid="healthProfile__medication"]')
        this.saveButton=page.locator('button[data-testid="healthProfile__saveButton"]')
    }

    async clearHealthProfileData(){
            await this.weight.selectText()
            await this.page.keyboard.press('Backspace')
            await this.height.selectText()
            await this.page.keyboard.press('Backspace')
            await this.allergies.selectText()
            await this.page.keyboard.press('Backspace')
            await this.diagnoses.selectText()
            await this.page.keyboard.press('Backspace')
            await this.surgeries.selectText()
            await this.page.keyboard.press('Backspace')
            await this.medication.selectText()
            await this.page.keyboard.press('Backspace')
    }

    async fillInTheHealthData(weight:string, height:string, allergies:string, diagnoses:string, surgeries:string, medication:string) {
        expect.soft(await this.weight.getAttribute('max')).toBe('700')
        expect.soft(await this.weight.getAttribute('min')).toBe('0')
        expect.soft(await this.weight.getAttribute('type')).toBe('number')
        expect.soft(await this.height.getAttribute('max')).toBe('300')
        expect.soft(await this.height.getAttribute('min')).toBe('0')
        expect.soft(await this.height.getAttribute('type')).toBe('number')
        await this.weight.fill(weight)
        await this.height.fill(height)
        await this.allergies.fill(allergies)
        await this.diagnoses.fill(diagnoses)
        await this.surgeries.fill(surgeries)
        await this.medication.type(medication)
        await this.saveButton.click()
    }
}