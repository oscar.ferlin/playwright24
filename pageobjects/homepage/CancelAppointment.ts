import { ElementHandle, expect, Locator, Page, Selectors } from "@playwright/test";
export class CancelAppointment{
readonly page:Page
readonly popUpSeeDetails: Locator
readonly removeRecommendation: Locator
readonly removeRecommendationYes: Locator
readonly exitDropInQueue: Locator
readonly cancelAppointmentButton:Locator
readonly yesCancelAppointmentBtn:Locator
readonly noCancelAppointmentBtn:Locator
readonly backToStartPageButton:Locator
readonly assertPlaceInQueue:Locator
readonly seeDetails:Locator


constructor(page:Page) {
    this.page=page
    this.popUpSeeDetails=page.locator('[data-testid="upcomingAppointmentModalV3"] [data-testid="see-details"]')
    this.removeRecommendation= page.locator('p[data-testid="recommendation-remove"]')
    this.removeRecommendationYes= page.locator('button[data-testid="modal-remove"]')
    this.exitDropInQueue= page.locator('a[href="/history"]')
    this.cancelAppointmentButton=page.locator('button[data-testid="appointment-cancel"]')
    this.yesCancelAppointmentBtn=page.locator('button[data-testid="modal-yes"]')
    this.noCancelAppointmentBtn=page.locator('button[data-testid="modal-cancel"]')
    this.backToStartPageButton=page.locator('article button[role="button"]')
    this.assertPlaceInQueue=page.locator('span[class*="WaitingRoomPosition"]')
    this.seeDetails=page.locator('button[data-testid="see-details"]')
    
}

//methods
async cancelTheAppointment() {
    await this.popUpSeeDetails.click()
    await this.cancelAppointmentButton.click()
    await this.yesCancelAppointmentBtn.click()
    await this.backToStartPageButton.click()
}

async cleanUpMyCase() {
    await this.seeDetails.click()
    await this.removeRecommendation.click()
    await this.removeRecommendationYes.click()
}
                                                                       
}    


/* if ((await page.$('[data-testid="upcomingAppointmentModalV3"] [data-testid="see-details"]')) !==null) {
    await cancelAppointment.cancelTheAppointment()
} */ 
