import { expect, Locator, Page, Selectors } from "@playwright/test";
export class PickOrigin{
    readonly page:Page
    readonly origins:Locator

    constructor(page:Page){
        this.page=page
        this.origins=page.locator('a')
    }

}
