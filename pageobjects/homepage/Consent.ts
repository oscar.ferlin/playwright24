import { expect, Locator, Page, Selectors } from "@playwright/test";
export class Consent{
    readonly page:Page
    readonly consentCheckBox: Locator
    readonly consentPage:Locator
    readonly acceptConsentBtn:Locator
    readonly consentTextInfo:Locator
    readonly consentYesRadioBtn: Locator
    readonly consentAgreeButton: Locator
    readonly acceptAccessPharmacyRecords: Locator
    readonly declineAccessPharmacyRecords: Locator
    readonly acceptAccessMedicalRecords: Locator
    readonly declineAccessMedicalRecords:Locator

    constructor(page:Page) {
    this.consentAgreeButton= page.locator('[aria-describedby="consents-descriptions"]')
    this.consentCheckBox=page.locator('(//input[@type="checkbox"])[1]')
    this.consentPage=page.locator('div[class*="styles_container"]')
    this.acceptConsentBtn=page.locator('button[role="button"]:visible')
    this.consentTextInfo=page.locator('#consents-descriptions')
    this.consentYesRadioBtn=page.locator('(//input[@type="radio"])[1]')
    this.acceptAccessPharmacyRecords=page.locator('#checkbox-ACCESS_PHARMACY_RECORDS-accept')
    this.declineAccessPharmacyRecords=page.locator('#checkbox-ACCESS_PHARMACY_RECORDS-decline')
    this.acceptAccessMedicalRecords=page.locator('#checkbox-ACCESS_MEDICAL_RECORDS-accept')
    this.declineAccessMedicalRecords=page.locator('#checkbox-ACCESS_MEDICAL_RECORDS-decline')

     }

     async acceptTheConsentEscalation() { 
        await expect.soft(this.acceptConsentBtn).toBeDisabled()
        await this.consentYesRadioBtn.click()
        await expect.soft(this.acceptConsentBtn).toBeEnabled()
        await this.acceptConsentBtn.click()
     }
}