import { expect, Locator, Page } from "@playwright/test";
export class FooterNavBar{

    readonly page: Page
    readonly home: Locator
    readonly myCase: Locator
    readonly myProfile: Locator
    readonly support: Locator

    constructor(page:Page){
        this.page=page
        this.home=page.locator('(//a[@data-testid="tab-bar-item"])[1]')
        this.myCase=page.locator('(//a[@data-testid="tab-bar-item"])[2]')
        this.myProfile=page.locator('(//a[@data-testid="tab-bar-item"])[3]')
        this.support=page.locator('(//a[@data-testid="tab-bar-item"])[4]') }

   async clickOnFooterMenu(tabName : string) {
       switch(tabName) {
          case 'home':
            await this.home.click() 
             break
          case 'myCase':
            await this.myCase.click()
             break  
          case 'myProfile':
            await this.myProfile.click()
             break 
          case 'support':
            await this.support.click()
             break
          default:
            throw new Error('This tab does not exist.')
                   

       }
   }     

}