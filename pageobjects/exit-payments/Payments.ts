import {Frame, FrameLocator, Locator, Page} from '@playwright/test'
export class Payments{
    readonly page: Page
    readonly freePass:Locator
    readonly goToKlarnaPayment:Locator
    readonly iframeCheckOut:FrameLocator
    readonly billingEmail:Locator
    readonly billingPostalCode:Locator
    readonly NextButton:Locator
    readonly goToPayment: Locator
    readonly iframeFullScreen:FrameLocator
    readonly payWithInvoice: Locator
    readonly checkOutBtn:Locator
    readonly thankYouForYourPaymentBtn: Locator
    readonly clickOnExistingFreePass:Locator
    readonly offlinePayment:Locator

    constructor(page: Page) {
        this.page=page
        const frame = this.iframeCheckOut=page.frameLocator('#klarna-checkout-iframe')
        this.billingEmail=frame.locator('#billing-email')
        this.billingPostalCode=frame.locator('#billing-postal_code')
        this.NextButton=frame.locator('#button-primary')
        this.goToPayment=frame.locator('button[data-cid="button.buy_button"]')
        const fullFrame= this.iframeFullScreen=page.frameLocator('#klarna-fullscreen-iframe')
        this.payWithInvoice=fullFrame.locator('input[value="klarna_category.invoice.-1"]')
        this.checkOutBtn=fullFrame.locator('button[data-cid="button.buy_button"]')
        this.thankYouForYourPaymentBtn=page.locator('button[data-testid="klarnaPayment_continueButton"]')
        this.freePass=page.locator('button[data-testid="freepass"]')
        this.goToKlarnaPayment=page.locator('footer button[role="button"]')
        this.clickOnExistingFreePass=page.locator('div[role="presentation"] button')
        this.offlinePayment=page.locator('footer button')
        
    }

    //click nextbutton 3 times. Once D24 has fixed payment task handler, we need to add the select payment method page aka freepass etc.. in the methods
    async payWithKlarnaInvoice(billingEmail:string, billingPostalCode:string) {
        this.iframeCheckOut
        await this.billingEmail.type(billingEmail)
        await this.billingPostalCode.type(billingPostalCode)
        for(let i=0;i<3;i++){
        await this.page.waitForLoadState('networkidle')
        await this.page.waitForTimeout(500)
        await this.NextButton.click() }
        await this.goToPayment.click()
        this.iframeFullScreen
        await this.payWithInvoice.click()
        await this.checkOutBtn.click()
        await this.thankYouForYourPaymentBtn.click()
    }

    async seekCareWithFreePass(){
        await this.freePass.click()
        await this.clickOnExistingFreePass.click()
    }
}
