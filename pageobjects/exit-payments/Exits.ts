import { expect, Locator, Page, Selectors } from "@playwright/test";
import{confirmContactDetails} from"../../commands"
export class Exits{

    readonly page:Page
    readonly seekCareOnlineBtn: Locator
    readonly dropInButton: Locator
    readonly bookButton: Locator
    readonly skipVideoTest:Locator
    readonly readSelfCareButton:Locator
    readonly changeCareUnitButton: Locator
    readonly doktor24CareUnit: Locator
    readonly pickTheFirstTimeSlotAvailable:Locator
    readonly sendTheAutoRefferalButton: Locator
    readonly patientRefferalTitle:Locator
    readonly elevenSeventySevenBtn:Locator

    constructor(page:Page){
        this.page=page
        this.seekCareOnlineBtn=page.locator('button[data-testid*="exit-action__option--seekCareOnline"]')
        this.dropInButton=page.locator('button[data-testid*="exit-action__option--seekCareOnlineDropIn"]')
        this.bookButton=page.locator('button[data-testid*="exit-action__option--seekCareOnlineScheduled"]')
        this.skipVideoTest=page.locator('a[href="#"]')
        this.readSelfCareButton=page.locator('button[data-testid="exit-action__option--selfCare"]')
        this.doktor24CareUnit= page.locator('button:has-text("Doktor24")')
        this.pickTheFirstTimeSlotAvailable=page.locator("div[data-testid='booking-dates-list-container'] button").first()
        this.changeCareUnitButton=page.locator('div[role="button"]')
        this.sendTheAutoRefferalButton=page.locator('button[data-testid="exit-action__option--patientReferral"]')
        this.patientRefferalTitle=page.locator('h3')
        this.elevenSeventySevenBtn=page.locator('button[data-testid*="call1177"]')
}

    async seekCareByDropIn() {
     await this.dropInButton.click()
    }

    async optOutExitBookAnAppointment() {
     await this.seekCareOnlineBtn.click()
     await this.changeCareUnitButton.click()
     await this.doktor24CareUnit.click()
     await this.pickTheFirstTimeSlotAvailable.click()
     await confirmContactDetails(this.page)
    }

    
}