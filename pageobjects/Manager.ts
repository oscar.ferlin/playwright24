import { Page } from "@playwright/test";
import{FooterNavBar} from "./components/FooterNavBar"
import{Exits} from "./exit-payments/Exits"
import {Payments} from "./exit-payments/Payments";
import { CancelAppointment } from "./homepage/CancelAppointment";
import { HomePage } from "./homepage/HomePage";
import { Myprofile } from "./homepage/Myprofile";
import {AcneDoktor24} from "./triage/AcneDoktor24";
import{PreTriage} from "./triage/Pre-triage";
import{PrescriptionRenewal} from "./triage/PrescriptionRenewal";
import { Psychology } from "./triage/Psychology";
//import{Springworm} from "./triage/Springworm";

export class Manager {
    readonly page: Page
    readonly footerNavBar: FooterNavBar 
    readonly exits: Exits
    readonly cancelAppointment: CancelAppointment
    readonly homePage: HomePage
    readonly prescriptionRenewal: PrescriptionRenewal
    readonly preTriage: PreTriage
    readonly acneDoktor24: AcneDoktor24
    readonly myProfile:Myprofile
    readonly payment: Payments
    readonly psychologyTriage:Psychology

    constructor(page:Page) {
    this.page=page
    this.footerNavBar= new FooterNavBar(page)
    this.exits= new Exits(page)
    this.payment= new Payments(page)
    this.cancelAppointment=new CancelAppointment(page)
    this.homePage= new HomePage(page)
    this.myProfile= new Myprofile(page)
    this.acneDoktor24= new AcneDoktor24(page)
    this.preTriage= new PreTriage(page)
    this.prescriptionRenewal= new PrescriptionRenewal(page)
    this.psychologyTriage=new Psychology(page)
   //this.Springworm= new Springworm(page) 
    }

    async getFooterNavBar() {
        return this.footerNavBar }

    async getExits() {
        return this.exits }    

    async getPayment() {
        return this.payment }
    async getCancelAppointment() {
        return this.cancelAppointment }
    async getHomePage() {
        return this.homePage }
    async getMyprofile() {
        return this.myProfile }
    async getAcneDoktor24() {
        return this.acneDoktor24 }
    async getPreTriage() {
        return this.preTriage }  
    async getPrescriptionRenewal() {
        return this.prescriptionRenewal }    
    async getPsychologyTriage() {
        return this.psychologyTriage }    


}
module.exports = {Manager}