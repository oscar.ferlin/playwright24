import {Locator, Page} from '@playwright/test'
export class AbstractTriage{
    readonly page: Page
    readonly freeTypeInputFinishInterview:Locator
    readonly nextFinishInterview:Locator
    readonly yesFinishInterview:Locator
    readonly noFinishInterview:Locator
    readonly ok:Locator
    readonly sendButton: Locator
    readonly no:Locator
    readonly inputSendButton:Locator
    readonly continueButton:Locator
    readonly yes:Locator
    readonly inputField:Locator
    readonly childInputSendButton:Locator
    readonly introMessagePartnerName:Locator
    

    constructor(page: Page) {
        this.page=page
        this.freeTypeInputFinishInterview=page.locator('input[data-testid="question-free-type-input"]')
        this.nextFinishInterview=page.locator('button[role="button"]:visible')
        this.yesFinishInterview=page.locator('(//input[@type="radio"])[1]')
        this.noFinishInterview=page.locator('(//input[@type="radio"])[2]')
        this.ok= page.locator('button[data-testid="continueChatButton"]')
        this.sendButton=page.locator('button[data-testid="sendButton"]')   
        this.no=page.locator('(//button[@data-testid="inputButton"])[2]')
        this.inputSendButton=page.locator('button[data-testid="inputSendButton"]')
        this.continueButton=page.locator('button[data-testid="endButton"]')
        this.yes=page.locator('(//button[@data-testid="inputButton"])[1]')
        this.inputField=page.locator('div[data-testid="input__wrapper"]')
        this.childInputSendButton=page.locator('button span svg')
        this.introMessagePartnerName=page.locator('(//div[@aria-live="polite"]//p)[1]')
    }   
}