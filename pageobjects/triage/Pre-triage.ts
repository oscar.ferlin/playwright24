import { expect, Locator, Page, Selectors } from "@playwright/test";
import { AbstractTriage } from "./AbstractTriage";

export class PreTriage extends AbstractTriage{

readonly page:Page
readonly inputFieldCondition:Locator
readonly searchResults: Locator
readonly seekCareForAdult: Locator
readonly seekCareForChild: Locator
readonly addChild: Locator
readonly nextButtonPsychology: Locator
readonly newPatientModelAdult:Locator
readonly askForPatientVisualTest:Locator
readonly maskOneSelfElementAskForPatient:Locator



constructor(page:Page) {
    super(page);
    this.inputFieldCondition=page.locator('input[data-testid="search-input"]')
    this.searchResults=page.locator('div[data-testid="search-results-container"] button')
    this.seekCareForAdult=page.locator('button[data-testid="oneself-patient"]')
    this.seekCareForChild=page.locator('(//button)[3]')
    this.addChild=page.locator('button[data-testid="add-child"]')
    this.nextButtonPsychology=page.locator('button[class*="MeetPsychologist"]')
    this.newPatientModelAdult=page.locator('(//button[@aria-disabled="false"])[1]')
    this.askForPatientVisualTest=page.locator('div[class*="styles_container"]')
    this.maskOneSelfElementAskForPatient=page.locator('(//button[@data-testid="oneself-patient"]//p)[1]')
}

async seekCareForAdultAndCondition(condition: string) {
    await this.inputFieldCondition.type(condition)
    await this.searchResults.first().waitFor()
    for(let i=0;i< await this.searchResults.count();i++) {
        if(await this.searchResults.nth(i).textContent()===condition) {
            await this.searchResults.nth(i).click()
        }}
    await this.seekCareForAdult.click()
}

async seekCareForChildAndCondition(condition: string) {
    await this.inputFieldCondition.type(condition)
    await this.searchResults.first().waitFor()
    for(let i=0;i< await this.searchResults.count();i++) {
        if(await this.searchResults.nth(i).textContent()===condition) {
            await this.searchResults.nth(i).click()
        }}
    await this.seekCareForChild.click()
}

}