import { expect, Locator, Page, Selectors } from "@playwright/test";
import { AbstractTriage } from "./AbstractTriage";

export class Psychology extends AbstractTriage {
    readonly page: Page
    readonly troubleSleeping:Locator
    readonly noneOfTheAbove1:Locator
    readonly noNotAParent:Locator
    readonly oneOrTwoNights:Locator
    readonly noneOfTheAbove2:Locator


    constructor(page:Page){
        super(page);
        this.troubleSleeping=page.locator('(//input[@type="radio"])[4]')
        this.noneOfTheAbove1= page.locator('//li[6]//label')
        this.noNotAParent=page.locator('(//button[@data-testid="inputButton"])[3]')
        this.oneOrTwoNights=page.locator('(//input[@type="radio"])[4]')
        this.noneOfTheAbove2= page.locator('//li[5]//label')
    }

    async navigateThroughTriage(page) {
        await this.ok.click()
        await page.waitForTimeout(2000)
        await this.ok.click()
        await this.troubleSleeping.check()
        await this.sendButton.click()
        await this.noneOfTheAbove1.check()
        await this.sendButton.click()
        await this.noNotAParent.click()
        await this.oneOrTwoNights.check()
        await this.sendButton.click()
        await this.no.click()
        await this.noneOfTheAbove2.click()
        await this.sendButton.click()
        await this.no.click()
        await this.no.click()
        await this.no.click()
        await this.no.click()
        await this.continueButton.click()

    }

    async navigateThroughPostTriage(){
        await this.no.click()
        await this.inputSendButton.click()
        await this.no.click()
        await this.noneOfTheAbove1.click()
        await this.sendButton.click()
        await this.no.click()
    }
}