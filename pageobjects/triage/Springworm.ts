import { expect, Locator, Page, Selectors } from "@playwright/test";
import { AbstractTriage } from "./AbstractTriage";
export class Springworm extends AbstractTriage{
    readonly page: Page;
    readonly inTheAnus: Locator
    readonly noneOfTheAbove: Locator
    readonly skinBlisters: Locator
    readonly otherDescription:Locator
    readonly diagnosis:Locator
    readonly hasNotSeenAnyWorms:Locator
    readonly finishInterviewNoneOfTheAbove:Locator


    constructor(page:Page){
        super(page);
        this.inTheAnus=page.locator('label:nth-child(3)')
        this.noneOfTheAbove=page.locator('li label').last()
        this.otherDescription=page.locator('li:nth-child(3) button:nth-child(1)')
        this.skinBlisters=page.locator('li:nth-child(4) label:nth-child(1)')
        this.diagnosis=page.locator('li:nth-child(1) label:nth-child(1)')
        this.hasNotSeenAnyWorms=page.locator('label:nth-child(4)')
        this.finishInterviewNoneOfTheAbove=page.locator('li:nth-child(5) label:nth-child(1)')

    }
    async navigateThroughTheTriage(){
        await this.ok.click()
        await this.no.click()
        await this.no.click()
        await this.inTheAnus.click()
        await this.sendButton.click()
        await this.noneOfTheAbove.click()
        await this.sendButton.click()
        await this.no.click()
        await this.otherDescription.click()
        await this.continueButton.click() }

    async navigateThroughThePostTriageFinishInterview(){
        await this.noFinishInterview.check()
        await this.nextFinishInterview.first().click()
        await this.freeTypeInputFinishInterview.type('Blueberry is awesome')
        await this.nextFinishInterview.first().click()
        await this.noFinishInterview.check()
        await this.nextFinishInterview.first().click()
        await this.finishInterviewNoneOfTheAbove.click()
        await this.nextFinishInterview.first().click()
        await this.noFinishInterview.check()
        await this.nextFinishInterview.first().click() }
    
    async getAutoRefferal(){
        await this.ok.click()
        await this.no.click()
        await this.no.click()
        await this.inTheAnus.click()
        await this.sendButton.click()
        await this.noneOfTheAbove.click()
        await this.sendButton.click()
        await this.yes.click()
        await this.otherDescription.click()
        await this.continueButton.click() }

    async navigateThroughChildTriage(){
        await this.ok.click()
        await this.inputField.type('5')
        await this.childInputSendButton.click()
        await this.no.click()
        await this.no.click()
        await this.inTheAnus.click()
        await this.sendButton.click()
        await this.noneOfTheAbove.click()
        await this.sendButton.click()
        await this.no.click()
        await this.otherDescription.click()
        await this.continueButton.click() } 

    async navigateThroughPostTriage(){
        await this.no.click()
        await this.inputSendButton.click()
        await this.no.click()
        await this.diagnosis.click()
        await this.sendButton.click()
        await this.no.click()  }

    async getPrio5(){
        await this.ok.click()
        await this.no.click()
        await this.no.click()
        await this.hasNotSeenAnyWorms.click()
        await this.sendButton.click()
        await this.noneOfTheAbove.click()
        await this.sendButton.click()
        await this.no.click()
        await this.no.click()
        await this.continueButton.click()
    }    

}