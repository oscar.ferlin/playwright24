import { expect, Locator, Page, Selectors } from "@playwright/test";
import { AbstractTriage } from "./AbstractTriage";
export class PrescriptionRenewal extends AbstractTriage{

    readonly getStartedButton:Locator
    readonly searchForPrescription:Locator
    readonly searchResults: Locator
    readonly canNotFindYourMedicineLink: Locator
    readonly skipMedicineAdnGoNextBtn: Locator
    readonly okButton: Locator
    readonly inputField: Locator
    readonly nextButton: Locator
    readonly regularyFinishInterview: Locator
    readonly whenNeededFinishInterview: Locator
    readonly weeksFinishInterview: Locator



    constructor(page:Page){
        super(page);
        this.getStartedButton=page.locator('button[data-testid="adult"]')
        this.searchForPrescription=page.locator('input[data-testid="search-input"]')
        this.searchResults=page.locator('div[data-testid="MedicationSearch__results"] button')
        this.canNotFindYourMedicineLink=page.locator('button[role="link"]')
        this.okButton=page.locator('button[role="button"]:has-text("Ok")')
        this.inputField=page.locator('input[data-testid="question-free-type-input"]')
        this.nextButton=page.locator('//div[contains(@class,"Question")]//button')
        this.skipMedicineAdnGoNextBtn=page.locator('button[type="button"] p')
        this.regularyFinishInterview=page.locator('(//input[@type="checkbox"])[1]')
        this.whenNeededFinishInterview=page.locator('(//input[@type="checkbox"])[2]')
        this.weeksFinishInterview=page.locator('(//input[@type="radio"])[2]')
    }

    async navigateThroughPrescriptionTriage() {
        await this.getStartedButton.click()
        // 2nd click on getstartbtn is to seek care for myself
        await this.getStartedButton.click()
        await this.canNotFindYourMedicineLink.click()
        await this.skipMedicineAdnGoNextBtn.click()
        await this.okButton.click()
        await this.nextButton.click() }

   async navigateThroughPostTriageFinishInterviewer(message:string){
       await this.freeTypeInputFinishInterview.type(message)
       await this.nextFinishInterview.click() 
       await this.noFinishInterview.check()
       await this.nextFinishInterview.click()
       await this.regularyFinishInterview.check()
       await this.nextFinishInterview.click()
       await this.weeksFinishInterview.hover()
       await this.weeksFinishInterview.click()
       await this.nextFinishInterview.click()
       await this.noFinishInterview.hover()
       await this.noFinishInterview.check() 
       await this.nextFinishInterview.click()

   }     
}