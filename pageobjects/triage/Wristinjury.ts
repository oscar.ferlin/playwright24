import { expect, Locator, Page, Selectors } from "@playwright/test";
import { AbstractTriage } from "./AbstractTriage";
export class Wristinjuty extends AbstractTriage{
    readonly page: Page
    readonly painRate8:Locator
    readonly severePain:Locator


    constructor(page: Page){
        super(page)
        this.painRate8=page.locator('label:nth-child(9)')
        this.severePain=page.locator('label:nth-child(4)')
    }


    async navigateThroughAdultTriage(){
      await this.ok.click()
      await this.painRate8.click()
      await this.sendButton.click()
      await this.yes.click()
      await this.yes.click()
      await this.continueButton.click()
    }

    async navigateThroughChildTriage(){
        await this.ok.click()
        await this.inputField.fill('5')
        await this.inputSendButton.click()
        await this.severePain.click()
        await this.sendButton.click()
        await this.yes.click()
        await this.yes.click()
        await this.continueButton.click()
    }
}