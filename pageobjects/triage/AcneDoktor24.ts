import { expect, Locator, Page, Selectors } from "@playwright/test";
import { AbstractTriage } from "./AbstractTriage";

export class AcneDoktor24 extends AbstractTriage{
readonly page:Page
readonly face:Locator
readonly skipButton:Locator
readonly noneOfTheAbove: Locator


constructor(page:Page) {
    super(page);
    this.face=page.locator('li:nth-child(1) label:nth-child(1)')
    this.skipButton=page.locator('button[data-testid="inputButton"]')
    this.noneOfTheAbove=page.locator('//li[6]//label[1]')

}

async navigateThroughAcneD24Triage() {
    await this.ok.click()
    await this.face.click()
    await this.sendButton.click()
    await this.skipButton.click()
    await this.skipButton.click()
    await this.no.click()
    await this.no.click()
    await this.noneOfTheAbove.click()
    await this.sendButton.click()
    await this.inputSendButton.click()
    await this.noneOfTheAbove.click()
    await this.sendButton.click()
    await this.continueButton.click()
}
}