import { PlaywrightTestConfig } from "@playwright/test";

const config: PlaywrightTestConfig = {

   timeout:90000,
    retries: 0,
    testDir: 'testCases/listing-integration',
    reporter:"html",
    use: {
        storageState:'./testCases/loginCookies/Norrbotten.json',
        trace: 'retain-on-failure',
        headless: true,
        //viewport: {width: 1280, height: 720},
        ignoreHTTPSErrors: false,
        actionTimeout: 10000,
        video:'retain-on-failure',
        screenshot:'only-on-failure'
    },
    expect: {
        timeout: 10000
      },
        projects: [ {
        name: 'chrome',
        use: {browserName:'chromium' }
    },
    
    {
        name: 'firefox',
        use: {browserName:'firefox' }
    }, 
    {
        name: 'webkit',
        use: {browserName:'webkit'  }
     
}],
}

export default config